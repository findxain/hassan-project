package com.example.hkhurshid.googlemap2;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
    public class View_Driver_challan extends AppCompatActivity {
        private RecyclerView recyclerView_driver;
        private accident_Reporting_view_Adapter adapter_driver;
        private List<Album> view_challan;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_view__driver_challan);

            //Parsing Recycler view
            recyclerView_driver = (RecyclerView) findViewById(R.id.recycler_view);
            //inializing Array

            view_challan = new ArrayList<>();
            adapter_driver = new accident_Reporting_view_Adapter(this);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
            recyclerView_driver.setLayoutManager(mLayoutManager);
            recyclerView_driver.setItemAnimator(new DefaultItemAnimator());
            recyclerView_driver.setAdapter(adapter_driver);



            try {
                Glide.with(this).load(R.drawable.police).into((ImageView) findViewById(R.id.backdrop));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Initializing collapsing toolbar
         * Will show and hide the toolbar title on scroll
         */



        }

