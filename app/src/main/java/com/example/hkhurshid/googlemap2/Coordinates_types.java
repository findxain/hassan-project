package com.example.hkhurshid.googlemap2;


public class Coordinates_types {
    public Double Latitude;
    public Double Longitude;
Coordinates_types(){
    setLatitude(0.0);
    setLongitude(0.0);
}

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }
}
