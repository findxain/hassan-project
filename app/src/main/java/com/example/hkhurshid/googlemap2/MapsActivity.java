package com.example.hkhurshid.googlemap2;

import android.Manifest;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Region;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hkhurshid.googlemap2.Utils.SharedPref;
import com.example.hkhurshid.googlemap2.Warden.CarInfo;
import com.google.android.gms.location.LocationListener;

import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private String path = "https://bolometric-fives.000webhostapp.com/Services/Warden.php";
    private String vechilesLocationUrl = "https://bolometric-fives.000webhostapp.com/Services/get_vechiles_location.php";
    private String wrdenZoneUrl = "https://bolometric-fives.000webhostapp.com/Services/get_warden_zones.php";

//    String temZOne = "{\"id\":\"22\",\"zone_id\":\"22\",\"warden_id\":\"1\",\"zone_name\":\"test7\",\"point_A_lat\":\"33.65208352936789\",\"point_A_lng\":\"73.07101249694824\",\"point_B_lat\":\"33.652735480745896\",\"point_B_lng\":\"73.07254672050476\",\"point_C_lat\":\"33.65068137063117\",\"point_C_lng\":\"73.07581901550293\",\"point_D_lat\":\"33.65007405912085\",\"point_D_lng\":\"73.07463884353638\",\"IsRes\":\"1\"}";

    public GoogleMap mMap;
    public EditText vehicleEdit, OwnerEdit;
    public static final int REQUEST_LOCATION_CODE = 99;
    private ProgressDialog pDialog;
    ArrayList<Coordinates> objListCor = new ArrayList<Coordinates>();
    public String TAG = "MapsActivity";

    RequestQueue queue;
    private ArrayList<CarInfo> cars;
    private ArrayList<Marker> carsMarkers;
    private SharedPref sharedPref;
    private ArrayList<LatLng> polygon;
    private ArrayList<String> chalanCars=new ArrayList<>();
    ArrayList<LatLng> Yellow9thLines2 = new ArrayList<>();
    PolylineOptions Yello9thline2;
   boolean isCalled = false;

    //PolyLines Updates Warden End
//    PolylineOptions line, line2,line3;
//    ArrayList<LatLng> lines = new ArrayList<>();
//    ArrayList<LatLng> lines2 = new ArrayList<>();
//    ArrayList<LatLng> lines3 = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        sharedPref=new SharedPref(this);
        queue = Volley.newRequestQueue(this);
        //Yello9thAve2
        Yellow9thLines2.add(new LatLng(33.655030, 73.069664));
        Yellow9thLines2.add(new LatLng(33.655337, 73.069399));
        Yellow9thLines2.add(new LatLng(33.655711, 73.069107));

        Yello9thline2 = new PolylineOptions().addAll(Yellow9thLines2)
                .width(32).color(Color.YELLOW);
//
//


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        carsMarkers = new ArrayList<>();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        doCallVehicleLocaions();


    }

    private void doCallVehicleLocaions() {
        StringRequest vehicleRequest = new StringRequest(Request.Method.GET, API.get_vechiles_location, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d(TAG, "onResponse: " + response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean error = jsonObject.getBoolean("error");

                    if (error) {
                        //Error
                    } else {
                        JSONArray arry = jsonObject.getJSONArray("v_details");
                        ArrayList<CarInfo> carInfos = new Gson().fromJson(arry.toString(), new TypeToken<List<CarInfo>>() {
                        }.getType());


                        showOnMap(carInfos);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                doCallVehicleLocaions();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                doCallVehicleLocaions();

            }
        });

        queue.add(vehicleRequest);


    }

    private void showOnMap(ArrayList<CarInfo> carInfos) {


        removeAllMarkers();


        this.cars = carInfos;


        for (CarInfo carInfo : cars) {
            LatLng carlatlng = new LatLng(
                    Double.parseDouble(carInfo.latitude), Double.parseDouble(carInfo.longitude)
            );
            MarkerOptions markerOptions = new MarkerOptions().position(carlatlng);
            markerOptions.title(carInfo.d_name);
            carsMarkers.add(mMap.addMarker(markerOptions));


            boolean isOnLine = PolyUtil.isLocationOnPath(new LatLng(Double.parseDouble(carInfo.latitude),Double.parseDouble(carInfo.longitude)), Yellow9thLines2, false, 15);

            if(isOnLine){
                Log.d(TAG, "Line Chanlan "+carInfo.toString());
                Toast.makeText(this, "Line Chanlan ", Toast.LENGTH_SHORT).show();
            }



//            boolean RedStadiumChallanline1 = PolyUtil.isLocationOnPath(new LatLng(carlatlng.getLatitude(), ca.getLongitude()), lines, false, 15);



//                if (Yello9thline2!=null && Yellow9thLines2.contains(carlatlng)) {
//                    if (isCalled) {
//
//                        Toast.makeText(this, "Welcome", Toast.LENGTH_SHORT).show();
//                        isCalled = true;
//                    }
//                }




            mMap.addPolyline(Yello9thline2);

            if (polygon!=null && isPointInPolygon(carlatlng,polygon)){

                Log.d(TAG,"Driver Ko Chalan ho gya "+carInfo.registration_no);

                if (chalanCars.contains(carInfo.registration_no)){

                }else {
                    chalanCars.add(carInfo.registration_no);



//                    doCallChalan(carInfo.vehicle_id,carInfo.owner_id);


                }
              }
        }


    }

    private void doCallChalan(final String vehicle_Id, final String ownerId) {
        StringRequest chanlanRequest=new StringRequest(Request.Method.POST, API.ChalanService, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject=new JSONObject(response);

                    String success=jsonObject.getString("success");
                    String message=jsonObject.getString("message");
                    Toast.makeText(MapsActivity.this, message, Toast.LENGTH_SHORT).show();




                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("vehicle_id",vehicle_Id);
                map.put("owner_id",ownerId);

                return map;
            }
        };
        queue.add(chanlanRequest);

    }

    private void removeAllMarkers() {
        for (Marker marker : carsMarkers) {
            marker.remove();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_LOCATION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                    } else {
                        Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

        showPolyGon();

    }

    private void showPolyGon() {
        try {
            JSONObject jsonObject = new JSONObject(sharedPref.getZone().toString());

            LatLng pointA = new LatLng(Double.parseDouble(jsonObject.getString("point_A_lat"))
                    , Double.parseDouble(jsonObject.getString("point_A_lng")));

            LatLng pointB = new LatLng(Double.parseDouble(jsonObject.getString("point_B_lat"))
                    , Double.parseDouble(jsonObject.getString("point_B_lng")));

            LatLng pointC = new LatLng(Double.parseDouble(jsonObject.getString("point_C_lat"))
                    , Double.parseDouble(jsonObject.getString("point_C_lng")));

            LatLng pointD = new LatLng(Double.parseDouble(jsonObject.getString("point_D_lat"))
                    , Double.parseDouble(jsonObject.getString("point_D_lng")));

            this.polygon=new ArrayList<LatLng>();
            polygon.add(pointA);
            polygon.add(pointB);
            polygon.add(pointC);
            polygon.add(pointD);

            int fillColor = Color.BLUE;

            Polygon polygon = mMap.addPolygon(new PolygonOptions()
                    .add(pointA, pointB, pointC, pointD)
                    .strokeColor(Color.RED)
                    .fillColor(Color.parseColor("#9999ff"))
                    .strokeWidth(10f));

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(pointA, 10)));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            }
            return false;
        } else
            return true;
    }


    public void btnSearch_OnClick(View v) {

        if (v.getId() == R.id.btnSearch) {
            EditText txtLocation = (EditText) findViewById(R.id.txtLocation);
            String Location = txtLocation.getText().toString();
            List<Address> addressList = null;
            MarkerOptions markerOptions = new MarkerOptions();
            if (!Location.equals("")) {
                Geocoder geocoder = new Geocoder(this);
                try {
                    addressList = geocoder.getFromLocationName(Location, 5);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < addressList.size(); i++) {
                    Address myAddress = addressList.get(i);
                    LatLng latLng = new LatLng(myAddress.getLatitude(), myAddress.getLongitude());
                    markerOptions.position(latLng);
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_mark));
                    markerOptions.title("Search result");
                    mMap.addMarker(markerOptions);
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                }
            }
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        queue.stop();
    }


    @Override
    protected void onResume() {
        super.onResume();
        queue.start();
    }



    private boolean isPointInPolygon(LatLng tap, ArrayList<LatLng> vertices) {
        int intersectCount = 0;
        for (int j = 0; j < vertices.size() - 1; j++) {
            if (rayCastIntersect(tap, vertices.get(j), vertices.get(j + 1))) {
                intersectCount++;
            }
        }

        return ((intersectCount % 2) == 1); // odd = inside, even = outside;
    }

    private boolean rayCastIntersect(LatLng tap, LatLng vertA, LatLng vertB) {

        double aY = vertA.latitude;
        double bY = vertB.latitude;
        double aX = vertA.longitude;
        double bX = vertB.longitude;
        double pY = tap.latitude;
        double pX = tap.longitude;

        if ((aY > pY && bY > pY) || (aY < pY && bY < pY)
                || (aX < pX && bX < pX)) {
            return false; // a and b can't both be above or below pt.y, and a or
            // b must be east of pt.x
        }

        double m = (aY - bY) / (aX - bX); // Rise over run
        double bee = (-aX) * m + aY; // y = mx + b
        double x = (pY - bee) / m; // algebra is neat!

        return x > pX;
    }



}


