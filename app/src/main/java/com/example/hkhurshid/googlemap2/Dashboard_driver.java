package com.example.hkhurshid.googlemap2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.hkhurshid.googlemap2.USER.UserMapActivity;

public class Dashboard_driver extends AppCompatActivity {

    Button Showmap,ViewChallan,Paychallan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_driver);

    Showmap = (Button)findViewById(R.id.showmap);
    Showmap.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(Dashboard_driver.this, UserMapActivity.class));
        }
    });


    ViewChallan = (Button)findViewById(R.id.viewchallan);
    ViewChallan.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(),View_Driver_challan.class);
            startActivity(intent);
        }
    });



    Paychallan = (Button)findViewById(R.id.paychallan);
    Paychallan.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {



        }
    });
    }
}
