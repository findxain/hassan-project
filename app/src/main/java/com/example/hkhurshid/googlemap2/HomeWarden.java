package com.example.hkhurshid.googlemap2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.hkhurshid.googlemap2.Utils.SharedPref;

public class HomeWarden extends AppCompatActivity {

    Button btnMaps,btnAccidentReporting,btnIncidentReporting,viewaccidentreporting,viewincidentreporting;
    public Double Userid;
    TextView txtName,txtShiftName,txtStartTiming,txtEndTiming,txtLoginTime;
    public UserWarden userWarden;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_warden);
        sharedPref=new SharedPref(this);

        txtName=(TextView) findViewById(R.id.txtName);
        txtShiftName=(TextView) findViewById(R.id.txtShiftName);
        txtStartTiming=(TextView) findViewById(R.id.txtStartTiming);
        txtEndTiming=(TextView) findViewById(R.id.txtEndTiming);
        txtLoginTime=(TextView) findViewById(R.id.txtLoginTime);


        userWarden=sharedPref.getWarden();

        txtName.setText(userWarden.Namme);
        txtShiftName.setText(userWarden.Shift_Name);
        txtStartTiming.setText(userWarden.Shift_Start);
        txtEndTiming.setText(userWarden.Shift_End);
        txtLoginTime.setText(userWarden.LoginTime);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnAccidentReporting =(Button)findViewById(R.id.accidentreporting);
        btnAccidentReporting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         Intent Accidentreport = new Intent(getApplicationContext(),DoAccidentReport.class);
         startActivity(Accidentreport);
            }
        });

        btnIncidentReporting = (Button)findViewById(R.id.incidentReporting);
        btnIncidentReporting.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent IncidentReport = new Intent(getApplicationContext(),DoIncidentReporting.class);
            startActivity(IncidentReport);
        }
        });






        viewaccidentreporting = (Button)findViewById(R.id.viewaccident_reporting);
        viewaccidentreporting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            Intent ViewAccidentR = new Intent(getApplicationContext(),AccidentReportingview.class);
                startActivity(ViewAccidentR);
            }
        });

        viewincidentreporting = (Button)findViewById(R.id.viewincidentreporting);
        viewincidentreporting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            Intent viewincidentR = new Intent(getApplicationContext(),Incident_View_Reporting_Grid.class);
            startActivity(viewincidentR);


            }
        });


    }
    public void showsmaps(View v) {
        btnMaps = (Button) findViewById(R.id.btnshowMaps);
        switch (v.getId()) {
            case R.id.btnshowMaps: {
                        Intent intent =null;
                        intent= new Intent(HomeWarden.this, MapsActivity.class);
                        intent.putExtra("UserId",Userid);
                        startActivity(intent);

            }

            break;
        }
    }
}


