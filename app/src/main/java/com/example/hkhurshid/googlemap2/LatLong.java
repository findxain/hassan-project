package com.example.hkhurshid.googlemap2;

/**
 * Created by Hassan Malik on 12/18/2017.
 */

public class LatLong {

    String lat , lng;

    public LatLong(String lat, String lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
