package com.example.hkhurshid.googlemap2;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
public class AccidentReportingview extends AppCompatActivity {

    //Declaration
    private RecyclerView recyclerView;
    private accident_Reporting_view_Adapter adapter;
    private RequestQueue queue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporting);


//        //Parsing
        queue = Volley.newRequestQueue(this);
//
//        initCollapsingToolbar();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//
//        data = new ArrayList<>();
        adapter = new accident_Reporting_view_Adapter(this);
//
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
//
        callService();
        }
//
        private void callService() {
        StringRequest s = new StringRequest(Request.Method.GET, API.GETACCEDENT_REPORT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



                Log.d("Responce", response);

                Type listType=new TypeToken<ArrayList<AccedentReport>>(){}.getType();
                ArrayList<AccedentReport> accedentReports=new Gson().fromJson(response,listType);
                adapter.setData(accedentReports);

            }




        }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                Toast.makeText(AccidentReportingview.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        queue.add(s);
    }

        public class AccedentReport {
        String id,Name,Challan_cat,Owner_id,vehicle_id,warden_id,challan_time,images,registration_no,permit_no,color,vhicle_category,is_assigned,zone_id,latitude,longitude,owner_id;

        ArrayList<String> image_paths;
        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }
//
//
//
//
//    private void initCollapsingToolbar() {
//        final CollapsingToolbarLayout collapsingToolbar =
//                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//        collapsingToolbar.setTitle(" ");
//        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
//        appBarLayout.setExpanded(true);
//
//        // hiding & showing the title when toolbar expanded & collapsed
//        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
//            boolean isShow = false;
//            int scrollRange = -1;
//
//            @Override
//            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                if (scrollRange == -1) {
//                    scrollRange = appBarLayout.getTotalScrollRange();
//                }
//                if (scrollRange + verticalOffset == 0) {
//                    collapsingToolbar.setTitle(getString(R.string.app_name));
//                    isShow = true;
//                } else if (isShow) {
//                    collapsingToolbar.setTitle(" ");
//                    isShow = false;
//                }
//            }
//        });
//    }
//    }


}


