package com.example.hkhurshid.googlemap2;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by h.khurshid on 12/8/2017.
 */

public class Coordinates {
    public String ZoneName;
    public Coordinates_types Point1;
    public Coordinates_types Point2;
    public Coordinates_types Point3;
    public Coordinates_types Point4;

    Coordinates(){
        ZoneName="";
    Point1= new Coordinates_types();
    Point2= new Coordinates_types();
    Point3= new Coordinates_types();
    Point4= new Coordinates_types();
    }
    public static PolygonOptions MakePolygon(Coordinates objCo, PolygonOptions PO){


        PO.add(new LatLng(objCo.Point1.Latitude,objCo.Point1.Longitude),new LatLng(objCo.Point2.Latitude,objCo.Point3.Longitude),new LatLng(objCo.Point3.Latitude,objCo.Point3.Longitude),new
                LatLng(objCo.Point4.Latitude,objCo.Point4.Longitude));
        return PO;

    }
    public static MarkerOptions  DrawMarker(Coordinates objCo,MarkerOptions MO){

        Double lat=0.0;
        Double lng=0.0;
        lat+=objCo.Point1.Latitude+objCo.Point2.Latitude+objCo.Point3.Latitude+objCo.Point4.Latitude;
        lng+=objCo.Point1.Longitude+objCo.Point2.Longitude+objCo.Point3.Longitude+objCo.Point4.Longitude;
        MO.title(objCo.ZoneName);
        MO.position(new LatLng(lat/4,lng/4));
        return MO;
    }
    public  static MarkerOptions vechilesLocation(Double lat,Double lng,Polygon PO,MarkerOptions MO){
            List<LatLng> p= PO.getPoints();
            LatLng l= new LatLng(lat,lng);
           // p.contains((objCo.Point1.Latitude));
        if( p.contains(l))
        {
            MO.position(new LatLng(lat,lng));
            MO.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_mark));

        }
        return MO;
    }



}
