package com.example.hkhurshid.googlemap2.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.hkhurshid.googlemap2.Model.User;
import com.example.hkhurshid.googlemap2.UserWarden;
import com.example.hkhurshid.googlemap2.Warden.Zone;
import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Zain on 12/24/2017.
 */

public class SharedPref {

    private static final String USER_WARDEN = "user_warden";
    private static final String ZONE = "zone";
    private Context context;
    SharedPreferences sharedPreferences;
    private String USER = "user";


    public SharedPref(Context context) {

        this.context = context;
        sharedPreferences = context.getSharedPreferences("pref", MODE_PRIVATE);

    }

    public void saveUser(User user) {
        sharedPreferences.edit().putString(USER, user.toString()).commit();
    }


    public User getUser() {
        return new Gson().fromJson(sharedPreferences.getString(USER, null), com.example.hkhurshid.googlemap2.Model.User.class);
    }

    public void saveWarden(UserWarden warden) {
        sharedPreferences.edit().putString(USER_WARDEN, warden.toString()).commit();

    }

    public UserWarden getWarden() {
        return new Gson().fromJson(sharedPreferences.getString(USER_WARDEN, null), UserWarden.class);
    }

    public void saveZone(Zone zone) {

        sharedPreferences.edit().putString(ZONE, zone.toString()).commit();

    }

    public Zone getZone() {

        return new Gson().fromJson(sharedPreferences.getString(ZONE, null), Zone.class);

    }

//    public SharedPreferences getSharedPreferences(String login, int modePrivate) {
//
//        sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
//
//        return sharedPreferences;
//    }
}
