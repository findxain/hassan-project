package com.example.hkhurshid.googlemap2;


public class Album {
    private String name,phone,liscence;
    private int numOfSongs;
    private int thumbnail,thumbnail2;

    public Album(String id, String image, String username, String phonenumber, String title, String description, String placebid, String category) {
    }

    public Album(String name,String phone,String liscence, int numOfSongs, int thumbnail, int thumbnail2) {

        this.name = name;
        this.phone = phone;
        this.liscence = liscence;
        this.numOfSongs = numOfSongs;
        this.thumbnail = thumbnail;
        this.thumbnail2 = thumbnail2;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public  String getPhone (){return phone; }

    public  void setPhone(String phone){this.phone = phone ;}


    public  String getLiscence (){return liscence; }

    public  void setLiscence(String liscence){this.liscence = liscence ;}


    public int getNumOfSongs() {
        return numOfSongs;
    }

    public void setNumOfSongs(int numOfSongs) {
        this.numOfSongs = numOfSongs;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getThumbnail2() {
        return thumbnail2;
    }

    public void setThumbnail2(int thumbnail2) {
        this.thumbnail2 = thumbnail2;
    }



}
