package com.example.hkhurshid.googlemap2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hkhurshid.googlemap2.Utils.Utils;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

public class DoAccidentReport extends AppCompatActivity {
    private static final int FIRST_IMAGE_REQUEST = 1;
    private static final int Second_IMAGE_REQUEST = 2;
    private static final int THird_IMAGE_REQUEST = 3;
    private Button choosebtn, uploadbtn;
    private EditText reg_no;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView imageView1, imageView2, imageView3;
    private final int IMG_REQUEST = 1;
    private Bitmap bitmap;

    String file1, file2, file3;
    private String TAG = "DoAccidentReport";
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_accident_report);



        choosebtn = (Button) findViewById(R.id.choosebtn);
        uploadbtn = (Button) findViewById(R.id.uploadbtn);
        reg_no = (EditText) findViewById(R.id.etregistration_no);
        imageView1 = (ImageView) findViewById(R.id.pic1);
        imageView2 = (ImageView) findViewById(R.id.pic2);
        imageView3 = (ImageView) findViewById(R.id.pic3);

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectfromcameraOrgallery(FIRST_IMAGE_REQUEST);
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectfromcameraOrgallery(Second_IMAGE_REQUEST);
            }
        });
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectfromcameraOrgallery(THird_IMAGE_REQUEST);
            }
        });


        uploadbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String regNo=reg_no.getText().toString();
                DochallanAccident(regNo);

            }
        });


        choosebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });
    }






    private void SelectfromcameraOrgallery(int requestCode) {
        //Selecting Image
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri path = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
            } catch (IOException e) {
                e.printStackTrace();
            }


            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imageView1.setImageBitmap(photo);
            }


            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imageView2.setImageBitmap(photo);
            }


            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imageView3.setImageBitmap(photo);
            }



            if (requestCode == FIRST_IMAGE_REQUEST) {
                file1 = Utils.SaveBitmapToFile(DoAccidentReport.this, bitmap);
                imageView1.setImageURI(Uri.parse(file1));

            } else if (requestCode == Second_IMAGE_REQUEST) {
                file2 = Utils.SaveBitmapToFile(DoAccidentReport.this, bitmap);
                imageView2.setImageURI(Uri.parse(file2));
            } else if (requestCode == THird_IMAGE_REQUEST) {
                file3 = Utils.SaveBitmapToFile(DoAccidentReport.this, bitmap);
                imageView3.setImageURI(Uri.parse(file3));
            }


        }
    }

    private void DochallanAccident(String registrationNo) {

        progressDialog=new ProgressDialog(DoAccidentReport.this);
        progressDialog.setMessage("Uploading Images");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);


        if (file1 != null && !file1.isEmpty()) {


            try {
                MultipartUploadRequest request = new MultipartUploadRequest(this, API.accident_challenge);
                request.addParameter("registration_no", registrationNo);
                if (file1 !=null && new File(file1).isFile()) {
                    request.addFileToUpload(file1, "image1");
                } else {
                    Toast.makeText(this, "Please Select an image", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (file2 !=null && new File(file2).isFile()) {
                    request.addFileToUpload(file2, "image2");
                }
                if (file3 !=null && new File(file3).isFile()) {
                    request.addFileToUpload(file3, "image3");
                }
                request.setNotificationConfig(null);
                request.setMaxRetries(3);
                request.setDelegate(new UploadServiceBroadcastReceiver()    {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        super.onReceive(context, intent);
                    }

                    @Override
                    protected boolean shouldAcceptEventFrom(UploadInfo uploadInfo) {
                        return super.shouldAcceptEventFrom(uploadInfo);
                    }

                    @Override
                    public void register(Context context) {
                        super.register(context);
                    }

                    @Override
                    public void unregister(Context context) {
                        super.unregister(context);
                    }

                    @Override
                    public void onProgress(Context context, UploadInfo uploadInfo) {
                        super.onProgress(context, uploadInfo);
                        Log.d(TAG, "onProgress: " + uploadInfo.getProgressPercent());
//                        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                        progressDialog.setIndeterminate(false);
                        progressDialog.setProgress(uploadInfo.getProgressPercent());
                    }

                    @Override
                    public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
                        super.onError(context, uploadInfo, serverResponse, exception);
                        exception.printStackTrace();
                    }

                    @Override
                    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
                        super.onCompleted(context, uploadInfo, serverResponse);
                        Log.d(TAG, "onCompleted: " + serverResponse.getBodyAsString());

                        progressDialog.setMessage(serverResponse.getBodyAsString());
//                        progressDialog.setCancelable(false);
                        progressDialog.setIndeterminate(true);

                    }

                    @Override
                    public void onCancelled(Context context, UploadInfo uploadInfo) {
                        super.onCancelled(context, uploadInfo);
                    }
                });

                request.startUpload();
                progressDialog.show();
                Toast.makeText(this, "Photos are Uploaded And Result is got successfully", Toast.LENGTH_SHORT).show();
//                progressDialog.setCancelable(false);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        } else {
            Toast.makeText(this, "Please select at least 1 image", Toast.LENGTH_SHORT).show();
        }



    }
}




