package com.example.hkhurshid.googlemap2;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Incident_View_Reporting_Grid extends AppCompatActivity {
    private RecyclerView recyclerView_inc;
    private incident_view_adapter inc_adapter;
    private RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.iew_incident_grid);
        //Parsing
        queue = Volley.newRequestQueue(this);
        //initCollapsingToolbar();
        recyclerView_inc = (RecyclerView) findViewById(R.id.recycler_view);
//       // data = new ArrayList<>();
        inc_adapter = new incident_view_adapter(this);

        recyclerView_inc = (RecyclerView) findViewById(R.id.recycler_view_inc);
       // inc_adapter = new accident_Reporting_view_Adapter(this);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView_inc.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView_inc.setAdapter(inc_adapter);

         callService();

    }

    private void callService() {
        StringRequest s = new StringRequest(Request.Method.GET, API.GETINCEDENT_REPORT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



                Log.d("Responce", response);

                Type listType=new TypeToken<ArrayList<Incidentreports>>(){}.getType();
                ArrayList<Incidentreports> Incidentreports=new Gson().fromJson(response,listType);
                inc_adapter.setData(Incidentreports);

            }




        }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                Toast.makeText(Incident_View_Reporting_Grid.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        queue.add(s);
    }

    public class Incidentreports {
        String id,severity_level;
        ArrayList<String> image_paths;
        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }

}


