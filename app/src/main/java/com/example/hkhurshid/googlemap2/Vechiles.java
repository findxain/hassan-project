package com.example.hkhurshid.googlemap2;

/**
 * Created by h.khurshid on 12/11/2017.
 */

public class Vechiles extends Coordinates_types {
    public String RegistrationNo;
    public String PermitNo;
    public String OwnerId;
    public String OwnerName;
    public String DriverName;
    public String Liscence_No;
    public String PhoneNumber;
    public String VehicleCategory;

    public Vechiles(String registrationNo, String permitNo, String ownerId, String ownerName, String driverName, String liscence_No, String phoneNumber, String vehicleCategory) {
        RegistrationNo = registrationNo;
        PermitNo = permitNo;
        OwnerId = ownerId;
        OwnerName = ownerName;
        DriverName = driverName;
        Liscence_No = liscence_No;
        PhoneNumber = phoneNumber;
        VehicleCategory = vehicleCategory;
    }

    @Override
    public String toString() {
        return "Vechiles{" +
                "RegistrationNo='" + RegistrationNo + '\'' +
                ", PermitNo='" + PermitNo + '\'' +
                ", OwnerId='" + OwnerId + '\'' +
                ", OwnerName='" + OwnerName + '\'' +
                ", DriverName='" + DriverName + '\'' +
                ", Liscence_No='" + Liscence_No + '\'' +
                ", PhoneNumber='" + PhoneNumber + '\'' +
                ", VehicleCategory='" + VehicleCategory + '\'' +
                '}';
    }

    public Vechiles(){
          RegistrationNo="";
          PermitNo="";
          OwnerId="";
          OwnerName="";
          DriverName="";
          Liscence_No="";
          PhoneNumber="";
          VehicleCategory="";
    }
}
