package com.example.hkhurshid.googlemap2.Warden;

import com.google.gson.Gson;

/**
 * Created by Hassan Malik on 12/26/2017.
 */

public class Zone {
    public String id,zone_id,warden_id,zone_name,point_A_lat,
            point_A_lng,point_B_lat,point_B_lng,point_C_lat,point_C_lng,
            point_D_lat,point_D_lng,IsRes;


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
