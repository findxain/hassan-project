package com.example.hkhurshid.googlemap2;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hkhurshid.googlemap2.Utils.SharedPref;
import com.example.hkhurshid.googlemap2.Warden.Zone;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.hkhurshid.googlemap2.API.UrlDriverLogin;
import static com.example.hkhurshid.googlemap2.API.UrlLogin;


public class Login extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button btnLogin;
    private Button forgetPass;
    private EditText txtName;
    private EditText txtPswd;
    Spinner cmboRoleid;
    JSONObject response_result= new JSONObject();
    Boolean error=true;
    Boolean timing_eror=false;
    String error_message="";
    int userid;
    private ProgressDialog pDialog;
    private JSONObject json;
    private HTTPURLConnection conn = new HTTPURLConnection();
    private String name_input ="", pass_input ="",role_id="";
    public UserWarden warden =null;
    RequestQueue queue;
    SharedPref sharedPref;
    private String TAG="Login";
    SharedPreferences prefs;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    
        boolean isLoggedIn = prefs.getBoolean("IsLoggedIn" , false);
        if(isLoggedIn)
        {
            Intent intent = new Intent(Login.this, HomeWarden.class);
            startActivity(intent);
        }
        
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        btnLogin=(Button) findViewById(R.id.btn_login);
        txtName=(EditText) findViewById(R.id.txtname);
        txtPswd=(EditText) findViewById(R.id.txtPassword);
        queue= Volley.newRequestQueue(Login.this);
        sharedPref=new SharedPref(this);
        cmboRoleid=(Spinner) findViewById(R.id.cmboRoles);
        cmboRoleid.setOnItemSelectedListener(this);
        ArrayList<String> Roles= new ArrayList<String>();
        Roles.add("Warden");
        Roles.add("Driver");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Roles);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cmboRoleid.setAdapter(dataAdapter);
    
        
    
    
    }
    public  void btnLogin_Click(View v){

        switch (v.getId()){
            case R.id.btn_login:{

                name_input=txtName.getText().toString();
                pass_input=txtPswd.getText().toString();
    
                prefs.edit().putBoolean("IsLoggedIn" , true).commit();


                        if(role_id.equalsIgnoreCase("1"))
                        {

//                            new PostDataTOServer().execute();
                            WardenLogin();

                        }

                        else
                        {

                            UserLogin();
                        }
                break;

            }

        }
        }

    private void UserLogin() {
        Log.v(Appconstants.TAG , "DRIVERRRR");


        pDialog = new ProgressDialog(Login.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlDriverLogin,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        // mTextView.setText("Response is: "+ response.substring(0,500));
                        Log.d(Appconstants.TAG, response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            boolean error = jsonObject.getBoolean("error");

                            if(!error)
                            {
                                JSONObject dataObj = jsonObject.getJSONObject("data");
                                com.example.hkhurshid.googlemap2.Model.User user=new Gson().fromJson(dataObj.toString(), com.example.hkhurshid.googlemap2.Model.User.class);
                                sharedPref.saveUser(user);


                                startActivity(new Intent(Login.this, Dashboard_driver.class));



                            }else {
                                Toast.makeText(Login.this,"Server Error",Toast.LENGTH_SHORT).show();
                            }

                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        pDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mTextView.setText("That didn't work!");
                error.printStackTrace();
                Log.e(Appconstants.TAG, error.toString());
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("name", name_input);
                map.put("password",pass_input);
                return map;
            }
        };
        stringRequest.setRetryPolicy((RetryPolicy) new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES + 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest
        );
    }
    private void WardenLogin() {
        Log.v(Appconstants.TAG , "Warden");


        pDialog = new ProgressDialog(Login.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST,API.UrlLogin,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        // mTextView.setText("Response is: "+ response.substring(0,500));
                        Log.d(Appconstants.TAG, response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            boolean error = jsonObject.getBoolean("error");

                            json = new JSONObject(response);

                            error=json.getBoolean("error");
                            timing_eror=json.getBoolean("timing_eror");

                            if(error==Boolean.TRUE){
                                error_message=json.getString("msg");
                            }else{

                                warden = new UserWarden();

                                warden.ID=json.getString("id");
                                warden.RoleName=json.getString("role_type");
                                warden.Namme=json.getString("name");
                                warden.Shift_Name=json.getString("shift_name");
                                warden.Shift_Start=json.getString("start_timing");
                                warden.Shift_End=json.getString("end_timing");
                                warden.LoginTime=new Date().toString();
                                sharedPref.saveWarden(warden);


                                JSONArray zonobjects=json.getJSONArray("v_details");

                                if (zonobjects.length()>0){


                                    Zone zone=new Gson().fromJson(zonobjects.getJSONObject(0).toString(),Zone.class);

                                    sharedPref.saveZone(zone);
                                }







                                Log.d("Responce", "onPostExecute: "+warden.toString());

                                Intent intent = new Intent(Login.this, HomeWarden.class);
                                startActivity(intent);


                            }

                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        pDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mTextView.setText("That didn't work!");
                error.printStackTrace();
                Log.e(Appconstants.TAG, error.toString());
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("name", name_input);
                map.put("password",pass_input);
                map.put("role_id","1");
                return map;
            }
        };
        stringRequest.setRetryPolicy((RetryPolicy) new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES + 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest
        );
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();
        if(item.equals("Warden")){
            role_id="1";
        }else {
            role_id="2";
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {

        String response = "";
        //Create hashmap Object to send parameters to web service
        HashMap<String, String> postDataParams;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(Login.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected Void doInBackground(Void... arg0) {
            postDataParams=new HashMap<String, String>();
            postDataParams.put("name", name_input);
            postDataParams.put("password", pass_input);
            postDataParams.put("role_id", role_id);

            //Call ServerData() method to call webservice and store result in response
            response= conn.ServerData(UrlLogin,postDataParams);
            try {
                Log.d(TAG, "doInBackground: "+response);

                json = new JSONObject(response);

                error=json.getBoolean("error");
                timing_eror=json.getBoolean("timing_eror");

                if(error==Boolean.TRUE){
                   error_message=json.getString("msg");
                }else{

                    warden = new UserWarden();

                    warden.ID=json.getString("id");
                    warden.RoleName=json.getString("role_type");
                    warden.Namme=json.getString("name");
                    warden.Shift_Name=json.getString("shift_name");
                    warden.Shift_Start=json.getString("start_timing");
                    warden.Shift_End=json.getString("end_timing");


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
                return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
            {
                pDialog.dismiss();

                if(error==true || timing_eror==true){

                    if(timing_eror==true){
                        Toast.makeText(Login.this, "Your Login timing is in correct", Toast.LENGTH_LONG).show();
                    }
                    if(error==true){
                        Toast.makeText(Login.this, error_message, Toast.LENGTH_LONG).show();
                    }

                }
                else {
                    if(warden !=null){
                        Intent intent =null;
                        if(warden.RoleName.equals("Warden")){

                            warden.LoginTime=new Date().toString();
                            sharedPref.saveWarden(warden);
                            Log.d("Responce", "onPostExecute: "+warden.toString());

                           intent= new Intent(Login.this, HomeWarden.class);
                            startActivity(intent);
                        }else
                        {

                        }
                    }
                }
            }
        }
    }
}
