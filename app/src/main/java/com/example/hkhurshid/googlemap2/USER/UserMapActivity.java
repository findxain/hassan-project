package com.example.hkhurshid.googlemap2.USER;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hkhurshid.googlemap2.API;
import com.example.hkhurshid.googlemap2.Model.User;
import com.example.hkhurshid.googlemap2.R;
import com.example.hkhurshid.googlemap2.Utils.SharedPref;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class UserMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String[] permsiions = {ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};
    private String TAG = "UserMapActivity";
    public User user;

    SharedPref sharedPref;
    RequestQueue queue;
    public   Boolean isCalled = false;
    PolylineOptions Yello9thline1,Yello9thline2,Yello9thline3,IjpLine2;
    ArrayList<LatLng> Yellow9thLines1 = new ArrayList<>();
    ArrayList<LatLng> Yellow9thLines2 = new ArrayList<>();
    ArrayList<LatLng> Yellow9thLines3 = new ArrayList<>();
    ArrayList<LatLng> IjpLines2 = new ArrayList<>();


    PolylineOptions line, NinthAveline,line3,stadiumline2,NinthAveline2,YellowstadiumLine1,YellowstadiumLine2;
    ArrayList<LatLng> lines = new ArrayList<>();
    ArrayList<LatLng> Stadiumlines = new ArrayList<>();
    ArrayList<LatLng> YellowstadiumLines1 = new ArrayList<>();
    ArrayList<LatLng> YellowstadiumLines2 = new ArrayList<>();
    ArrayList<LatLng> NinthAvelines1 = new ArrayList<>();
    ArrayList<LatLng> NinthAvelines2 = new ArrayList<>();
    ArrayList<LatLng> lines3 = new ArrayList<>();


    //    private FusedLocationProviderClient mFusedLocationClient;
//    private LocationCallback mLocationCallback;
//    LocationRequest mLocationRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        queue = Volley.newRequestQueue(this);
        sharedPref = new SharedPref(this);

//Yello9thAve1
        Yellow9thLines1.add(new LatLng(33.654602, 73.068804));
        Yellow9thLines1.add(new LatLng(33.654729, 73.068956));
        Yellow9thLines1.add(new LatLng(33.654809, 73.069023));
        Yellow9thLines1.add(new LatLng(33.654930, 73.069075));
        Yellow9thLines1.add(new LatLng(33.655097, 73.069115));
        Yellow9thLines1.add(new LatLng(33.655286, 73.069146));
        Yellow9thLines1.add(new LatLng(33.655481, 73.069127));
        Yellow9thLines1.add(new LatLng(33.655625, 73.069091));
        Yellow9thLines1.add(new LatLng(33.655703, 73.069065));

        Yello9thline1 = new PolylineOptions().addAll(Yellow9thLines1)
                .width(32).color(Color.YELLOW);

        //Yello9thAve2
        Yellow9thLines2.add(new LatLng(33.655030, 73.069664));
        Yellow9thLines2.add(new LatLng(33.655337, 73.069399));
        Yellow9thLines2.add(new LatLng(33.655711, 73.069107));

        Yello9thline2 = new PolylineOptions().addAll(Yellow9thLines2)
                .width(32).color(Color.YELLOW);


        //Yello9thAve2
        Yellow9thLines3.add(new LatLng(33.655069, 73.069817));
        Yellow9thLines3.add(new LatLng(33.655244, 73.069672));
        Yellow9thLines3.add(new LatLng(33.655466, 73.069493));
        Yellow9thLines3.add(new LatLng(33.655784, 73.069237));


        Yello9thline3 = new PolylineOptions().addAll(Yellow9thLines3)
                .width(32).color(Color.YELLOW);




        //IjpLine1 array about lat long
        lines3.add(new LatLng(33.650442, 73.061329));
        lines3.add(new LatLng(33.650704, 73.061814));
        lines3.add(new LatLng(33.650910, 73.062193));
        lines3.add(new LatLng(33.651048, 73.062455));
        lines3.add(new LatLng(33.651249, 73.062814));
        lines3.add(new LatLng(33.651393, 73.063090));
        lines3.add(new LatLng(33.651048, 73.062455));
        lines3.add(new LatLng(33.651680, 73.063643));
        lines3.add(new LatLng(33.651828, 73.063921));
        lines3.add(new LatLng(33.651973, 73.064200));
        lines3.add(new LatLng(33.652113, 73.064445));
        lines3.add(new LatLng(33.652248, 73.064664));
        lines3.add(new LatLng(33.652432, 73.065028));
        lines3.add(new LatLng(33.652530, 73.065242));
        lines3.add(new LatLng(33.652657, 73.065514));
        lines3.add(new LatLng(33.652791, 73.065797));
        lines3.add(new LatLng(33.652930, 73.066082));
        lines3.add(new LatLng(33.653059, 73.066341));
        lines3.add(new LatLng(33.653215, 73.066650));
        lines3.add(new LatLng(33.653421, 73.067051));
        lines3.add(new LatLng(33.653555, 73.067312));
        lines3.add(new LatLng(33.653796, 73.067777));
        lines3.add(new LatLng(33.653938, 73.068059));
        lines3.add(new LatLng(33.654133, 73.068434));
        lines3.add(new LatLng(33.654376, 73.068895));
        lines3.add(new LatLng(33.654531, 73.069173));
        lines3.add(new LatLng(33.654841, 73.069722));
        lines3.add(new LatLng(33.654969, 73.069986));
        lines3.add(new LatLng(33.655171, 73.070380));
        lines3.add(new LatLng(33.655366, 73.070754));
        lines3.add(new LatLng(33.655512, 73.071035));
        lines3.add(new LatLng(33.655658, 73.071315));
        lines3.add(new LatLng(33.655904, 73.071784));
        lines3.add(new LatLng(33.656147, 73.072250));
        lines3.add(new LatLng(33.656488, 73.072901));
        lines3.add(new LatLng(33.656685, 73.073274));
        lines3.add(new LatLng(33.656875, 73.073619));
        lines3.add(new LatLng(33.657134, 73.074106));
        lines3.add(new LatLng(33.657478, 73.074764));


//
        line3 = new PolylineOptions().addAll(lines3)
                .width(50).color(Color.GREEN);


        //Ijp Lines 2 Bahria Town Dastar Khawan To Mid city Hotel


        //IjpLine2 array about lat long
        IjpLines2.add(new LatLng(33.657578, 73.074629));
        IjpLines2.add(new LatLng(33.657359, 73.074225));
        IjpLines2.add(new LatLng(33.657144, 73.073824));
        IjpLines2.add(new LatLng(33.656877, 73.073326));
        IjpLines2.add(new LatLng(33.656768, 73.073122));
        IjpLines2.add(new LatLng(33.656664, 73.072920));
        IjpLines2.add(new LatLng(33.656240, 73.072117));
        IjpLines2.add(new LatLng(33.656023, 73.071708));
        IjpLines2.add(new LatLng(33.655812, 73.071306));
        IjpLines2.add(new LatLng(33.655649, 73.070999));
        IjpLines2.add(new LatLng(33.655492, 73.070702));
        IjpLines2.add(new LatLng(33.655281, 73.070298));
        IjpLines2.add(new LatLng(33.655045, 73.069831));
        IjpLines2.add(new LatLng(33.654875, 73.069480));
        IjpLines2.add(new LatLng(33.654685, 73.069061));
        IjpLines2.add(new LatLng(33.654540, 73.068746));
        IjpLines2.add(new LatLng(33.654377, 73.068447));
        IjpLines2.add(new LatLng(33.654212, 73.068151));
        IjpLines2.add(new LatLng(33.653994, 73.067755));
        IjpLines2.add(new LatLng(33.653770, 73.067351));
        IjpLines2.add(new LatLng(33.653606, 73.067056));
        IjpLines2.add(new LatLng(33.653444, 73.066757));
        IjpLines2.add(new LatLng(33.653290, 73.066473));
        IjpLines2.add(new LatLng(33.653065, 73.066047));
        IjpLines2.add(new LatLng(33.652914, 73.065759));
        IjpLines2.add(new LatLng(33.652834, 73.065528));
        IjpLines2.add(new LatLng(33.652773, 73.065361));
        IjpLines2.add(new LatLng(33.652581, 73.065024));
        IjpLines2.add(new LatLng(33.652481, 73.064818));
        IjpLines2.add(new LatLng(33.652432, 73.064694));
        IjpLines2.add(new LatLng(33.652327, 73.064553));
        IjpLines2.add(new LatLng(33.652048, 73.064019));
        IjpLines2.add(new LatLng(33.651899, 73.063716));
        IjpLines2.add(new LatLng(33.651477, 73.062905));
        IjpLines2.add(new LatLng(33.651134, 73.062246));
        IjpLines2.add(new LatLng(33.650927, 73.061852));
        IjpLines2.add(new LatLng(33.650682, 73.061386));
        IjpLines2.add(new LatLng(33.650573, 73.061180));
//        IjpLines2.add(new LatLng(33.657478, 73.074764));


//
        IjpLine2 = new PolylineOptions().addAll(IjpLines2)
                .width(50).color(Color.GREEN);





        //9th AvenueLine1 array about lat long
        NinthAvelines1.add(new LatLng(33.655706, 73.069084));
        NinthAvelines1.add(new LatLng(33.656304, 73.068692));
        NinthAvelines1.add(new LatLng(33.656875, 73.068252));
        NinthAvelines1.add(new LatLng(33.657102, 73.068084));


        NinthAveline = new PolylineOptions().addAll(NinthAvelines1)
                .width(42).color(Color.RED);


        //9th AvenueLine2 array about lat long
        NinthAvelines2.add(new LatLng(33.655784, 73.069237));
        NinthAvelines2.add(new LatLng(33.655961, 73.069094));
        NinthAvelines2.add(new LatLng(33.656307, 73.068851));
        NinthAvelines2.add(new LatLng(33.656978, 73.068514));


        NinthAveline2 = new PolylineOptions().addAll(NinthAvelines2)
                .width(32).color(Color.RED);


        //StadiumLine1 array about lat long
        lines.add(new LatLng(33.654346, 73.070151));
        lines.add(new LatLng(33.654055, 73.070391));
        lines.add(new LatLng(33.653794, 73.070607));
        lines.add(new LatLng(33.653505, 73.070827));
        lines.add(new LatLng(33.653378, 73.070923));
        lines.add(new LatLng(33.653288, 73.070975));
        lines.add(new LatLng(33.653148, 73.071069));
        lines.add(new LatLng(33.653002, 73.071174));
        lines.add(new LatLng(33.652858, 73.071276));
        lines.add(new LatLng(33.652648, 73.071430));
        lines.add(new LatLng(33.652512, 73.071554));
        lines.add(new LatLng(33.652369, 73.071692));
        lines.add(new LatLng(33.652123, 73.071923));
        lines.add(new LatLng(33.651868, 73.072179));
        lines.add(new LatLng(33.651690, 73.072392));
        lines.add(new LatLng(33.651519, 73.072607));
        lines.add(new LatLng(33.651264, 73.073002));
        lines.add(new LatLng(33.651013, 73.073394));
        lines.add(new LatLng(33.650842, 73.073715));
        lines.add(new LatLng(33.650637, 73.074169));
        lines.add(new LatLng(33.650497, 73.074585));
        lines.add(new LatLng(33.650267, 73.075386));
        lines.add(new LatLng(33.650004, 73.076326));
        lines.add(new LatLng(33.649830, 73.076968));
        lines.add(new LatLng(33.649616, 73.077749));
        lines.add(new LatLng(33.649451, 73.078364));
        lines.add(new LatLng(33.649250, 73.079162));
        lines.add(new LatLng(33.649176, 73.079446));
        lines.add(new LatLng(33.649116, 73.079525));
        lines.add(new LatLng(33.649049, 73.079595));
        lines.add(new LatLng(33.648987, 73.079626));


        //        Iterable<LatLng> iterable = lines;
        line = new PolylineOptions().addAll(lines)
                .width(28).color(Color.RED);

        //Stadium Lines 2
        Stadiumlines.add(new LatLng(33.654402, 73.070300));
        Stadiumlines.add(new LatLng(33.654131, 73.070504));
        Stadiumlines.add(new LatLng(33.653846, 73.070723));
        Stadiumlines.add(new LatLng(33.653571, 73.070935));
        Stadiumlines.add(new LatLng(33.653292, 73.071147));
        Stadiumlines.add(new LatLng(33.653159, 73.071263));
        Stadiumlines.add(new LatLng(33.653022, 73.071364));
        Stadiumlines.add(new LatLng(33.652744, 73.071576));
        Stadiumlines.add(new LatLng(33.652415, 73.071868));
        Stadiumlines.add(new LatLng(33.652096, 73.072155));
        Stadiumlines.add(new LatLng(33.651966, 73.072286));
        Stadiumlines.add(new LatLng(33.651732, 73.072574));
        Stadiumlines.add(new LatLng(33.651512, 73.072873));
        Stadiumlines.add(new LatLng(33.651416, 73.073007));
        Stadiumlines.add(new LatLng(33.651168, 73.073407));
        Stadiumlines.add(new LatLng(33.651048, 73.073602));
        Stadiumlines.add(new LatLng(33.650940, 73.073828));
        Stadiumlines.add(new LatLng(33.650800, 73.074123));
        Stadiumlines.add(new LatLng(33.650643, 73.074512));
        Stadiumlines.add(new LatLng(33.650581, 73.074701));
        Stadiumlines.add(new LatLng(33.650499, 73.074978));
        Stadiumlines.add(new LatLng(33.650379, 73.075465));
        Stadiumlines.add(new LatLng(33.650240, 73.076011));
        Stadiumlines.add(new LatLng(33.650125, 73.076431));
        Stadiumlines.add(new LatLng(33.649961, 73.077039));
        Stadiumlines.add(new LatLng(33.649854, 73.077416));
        Stadiumlines.add(new LatLng(33.649725, 73.077877));
        Stadiumlines.add(new LatLng(33.649617, 73.078263));
        Stadiumlines.add(new LatLng(33.649459, 73.078824));
        Stadiumlines.add(new LatLng(33.649291, 73.079415));
        Stadiumlines.add(new LatLng(33.649285, 73.079511));
        Stadiumlines.add(new LatLng(33.649316, 73.079655));
        Stadiumlines.add(new LatLng(33.649353, 73.079758));


        stadiumline2 = new PolylineOptions().addAll(Stadiumlines)
                .width(32).color(Color.RED);

        //YellowStadiumLine1
        YellowstadiumLines1.add(new LatLng(33.654831, 73.069787));
        YellowstadiumLines1.add(new LatLng(33.654546, 73.070005));
        YellowstadiumLines1.add(new LatLng(33.654346, 73.070153));

        YellowstadiumLine1 = new PolylineOptions().addAll(YellowstadiumLines1)
                .width(32).color(Color.YELLOW);



        //YellowStadiumLine2
        YellowstadiumLines2.add(new LatLng(33.654901, 73.069939));
        YellowstadiumLines2.add(new LatLng(33.654613, 73.070146));
        YellowstadiumLines2.add(new LatLng(33.654408, 73.070295));

        YellowstadiumLine2 = new PolylineOptions().addAll(YellowstadiumLines2)
                .width(32).color(Color.YELLOW);



        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (mFusedLocationClient != null) {
//            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, permsiions, 30);
        } else {
            setupLocationListners();
        }


    }

    private void setupLocationListners() {

//        mLocationCallback = new LocationCallback() {
//            @Override
//            public void onLocationResult(LocationResult locationResult) {
//                for (Location location : locationResult.getLocations()) {
//                    // Update UI with location data
//                    // ...
//                    Log.d("location", location.getLatitude() + "/" + location.getLongitude());
//
////                    updateLocation();
//
//
//                }
//            }
//
//            ;
//        };
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
//                .addLocationRequest(createLocationRequest());
//        SettingsClient client = LocationServices.getSettingsClient(this);
//
//        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
//
//        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
//            @Override
//            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
//                // All location settings are satisfied. The client can initialize
//                // location requests here.
//                // ...
//                Log.d(TAG, "onSuccess: Location Setup Succeddd");
//
//
//                requestLocationUpdates();
//
//
//            }
//        });
//        task.addOnFailureListener(this, new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                e.printStackTrace();
//                Log.d(TAG, "onFailure: Location Setup Failed");
//
//            }
//        });


    }

    private void requestLocationUpdates() {
//        if (ActivityCompat.checkSelfPermission(UserMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UserMapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//
//        } else {
//            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
//                    mLocationCallback,
//                    null);
//        }
    }

    //    protected LocationRequest createLocationRequest() {
////        mLocationRequest = new LocationRequest();
////        mLocationRequest.setInterval(10000);
////        mLocationRequest.setFastestInterval(5000);
////        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
////        return mLocationRequest;
//    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onMyLocationChange(Location location) {

                Log.d("GoogleLocation", "onMyLocationChange: " + location.getLatitude() + "/" + location.getLongitude());
                doCallLocationUpdate(location);


                boolean RedStadiumChallanline1 = PolyUtil.isLocationOnPath(new LatLng(location.getLatitude(), location.getLongitude()), lines, false, 15);
                boolean RedStadiumChallanline2 = PolyUtil.isLocationOnPath(new LatLng(location.getLatitude(), location.getLongitude()), Stadiumlines, false, 15);
                boolean Red9thAveneuchallanhallanlines1 = PolyUtil.isLocationOnPath(new LatLng(location.getLatitude(), location.getLongitude()), NinthAvelines1, false, 15);
                boolean Red9thAveneuchallanlines2 = PolyUtil.isLocationOnPath(new LatLng(location.getLatitude(), location.getLongitude()), NinthAvelines2, false, 15);

                //Warnings
                boolean YellowLine1StadiumWarning = PolyUtil.isLocationOnPath(new LatLng(location.getLatitude(), location.getLongitude()), YellowstadiumLines1, false, 15);
                boolean YellowLine2StadiumWarning = PolyUtil.isLocationOnPath(new LatLng(location.getLatitude(), location.getLongitude()), YellowstadiumLines2, false, 15);
                boolean YellowLine19thAveLine1Warning = PolyUtil.isLocationOnPath(new LatLng(location.getLatitude(), location.getLongitude()), Yellow9thLines1, false, 15);
                boolean YellowLine19thAveLine2Warning = PolyUtil.isLocationOnPath(new LatLng(location.getLatitude(), location.getLongitude()), Yellow9thLines2, false, 15);
                boolean YellowLine19thAveLine3Warning = PolyUtil.isLocationOnPath(new LatLng(location.getLatitude(), location.getLongitude()), Yellow9thLines3, false, 15);


                User user = sharedPref.getUser();

                if (RedStadiumChallanline1) {

                    if (!isCalled) {
                        doCallChalan(user.vehicle_id, user.owner_id);
//                        CallAutoNotification();
                        isCalled = true;

                    }
                    Toast.makeText(UserMapActivity.this, "Unfortunately challan created u have Touched Stadium Road 1" + user.vehicle_id + user.vehicle_category, Toast.LENGTH_SHORT).show();
                } else if (RedStadiumChallanline2) {

                    if (!isCalled) {
                        doCallChalan(user.vehicle_id, user.owner_id);
//                        CallAutoNotification();
                        Toast.makeText(UserMapActivity.this, "Unfortunately challan created u have Touched Stadium Road 2", Toast.LENGTH_SHORT).show();
                        isCalled = true;
                    }
                }
                else if (Red9thAveneuchallanhallanlines1) {

                    if (!isCalled) {
                        doCallChalan(user.vehicle_id, user.owner_id);
//                        CallAutoNotification();
                        Toast.makeText(UserMapActivity.this, "Unfortunately challan created u have Touched 9th Ave Line 1", Toast.LENGTH_SHORT).show();
                        isCalled = true;
                    }
                }
                else if (Red9thAveneuchallanlines2) {

                    if (!isCalled) {
                        doCallChalan(user.vehicle_id, user.owner_id);
//                        CallAutoNotification();
                        Toast.makeText(UserMapActivity.this, "Unfortunately challan created u have Touched 9th Ave Line 2", Toast.LENGTH_SHORT).show();
                        isCalled = true;
                    }
                }
                else if (YellowLine1StadiumWarning || YellowLine2StadiumWarning  || YellowLine19thAveLine1Warning || YellowLine19thAveLine2Warning  || YellowLine19thAveLine3Warning ) {

                    if (!isCalled) {
                        Toast.makeText(UserMapActivity.this, "Warning !!!"+user.d_name+"You Are Moving Towards Restricted Area ", Toast.LENGTH_SHORT).show();
                        isCalled = true;
                    }
                }
            }
        });

        enableMyLocation();


        //9th Ave Yellow
        mMap.addPolyline(Yello9thline1);
        mMap.addPolyline(Yello9thline2);
        mMap.addPolyline(Yello9thline3);

        //9th Ave Red
        mMap.addPolyline(NinthAveline);
        mMap.addPolyline(NinthAveline2);
        //Stadium Road Red
        mMap.addPolyline(line);
        mMap.addPolyline(stadiumline2);
        //Granted poly Line

        //Ijp Road GREEN
        mMap.addPolyline(line3);
        mMap.addPolyline(IjpLine2);

        //yellowLinestadium
        mMap.addPolyline(YellowstadiumLine1);
        mMap.addPolyline(YellowstadiumLine2);






        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(line.getPoints().get(0), 14));


//        LatLngBounds latLngBounds = new LatLngBounds(line.getPoints().get(0),line.getPoints().get(1));
//        mMap.setLatLngBoundsForCameraTarget(latLngBounds);



    }


//    public void CallAutoNotification(){
//
//            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder builder=new NotificationCompat.Builder(this);
//            builder.setSmallIcon(R.drawable.police);
//            builder.setContentTitle("Violation Notification");
//            builder.setContentText("You have Violated traffic laws and Challan is generated against you");
//            builder.setSound(defaultSoundUri);
//            builder.setAutoCancel(true);
////            Intent intent=new Intent(this,SecondClass.class);
//            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//            stackBuilder.addParentStack(UserMapActivity.this);
////            stackBuilder.addNextIntent(getApplicationContext());
//            PendingIntent pendingIntent=stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
//            builder.setContentIntent(pendingIntent);
//            NotificationManager NM= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            NM.notify(0,builder.build());
//
//
//        }


    private void doCallLocationUpdate(final Location location) {


        StringRequest locationUpdaeRequest = new StringRequest(Request.Method.POST, API.DRIVER_LOCATION_UPDATE_URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d(TAG, "onResponse: " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("latitude", location.getLatitude() + "");
                map.put("longitude", location.getLongitude() + "");
                User user = sharedPref.getUser();
                map.put("id", user.d_id + "");

                Log.d(TAG, "getParams: " + map.toString());

                return map;
            }
        };
        queue.add(locationUpdaeRequest);
    }


    private void doCallChalan(final String vehicle_Id, final String ownerId) {
        StringRequest chanlanRequest = new StringRequest(Request.Method.POST, API.ChalanService, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String success = jsonObject.getString("success");
                    String message = jsonObject.getString("message");
                    Toast.makeText(UserMapActivity.this, message, Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("vehicle_id", vehicle_Id);
                map.put("owner_id", ownerId);

                return map;
            }
        };
        queue.add(chanlanRequest);

    }

    private void enableMyLocation() {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, permsiions, 30);
        } else {
            mMap.setMyLocationEnabled(true);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 30) {
            if (grantResults != null && grantResults.length > 0) {
                enableMyLocation();
            } else {
                Toast.makeText(UserMapActivity.this, "Location permission Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
