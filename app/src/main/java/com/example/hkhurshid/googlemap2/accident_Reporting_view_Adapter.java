package com.example.hkhurshid.googlemap2;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
public class accident_Reporting_view_Adapter extends RecyclerView.Adapter<accident_Reporting_view_Adapter.MyViewholder> {
    private Context mContext;
    private ArrayList<AccidentReportingview.AccedentReport> data;

    public void setData(ArrayList<AccidentReportingview.AccedentReport> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    //view Holder class which holds the view
    public class MyViewholder extends RecyclerView.ViewHolder {
        public TextView Name, phone,liscence;
        public ImageView imageView, imageView2,imageview3;

        //Constructor of view holder
        public MyViewholder(View itemView) {
            super(itemView);
            Name = (TextView) itemView.findViewById(R.id.textViewName);
            phone = (TextView) itemView.findViewById(R.id.textPhone);
            liscence = (TextView) itemView.findViewById(R.id.textViewliscence);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imageView2 = (ImageView) itemView.findViewById(R.id.imageView2);
            imageview3 = (ImageView) itemView.findViewById(R.id.imageView3);
        }
    }

    public accident_Reporting_view_Adapter(Context mContext) {
        this.mContext = mContext;

    }

    @Override
    public accident_Reporting_view_Adapter.MyViewholder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_accident__reporting_view__adapter, parent, false);

        return new MyViewholder(itemView);
    }
    @Override
    public void onBindViewHolder(final accident_Reporting_view_Adapter.MyViewholder holder, int position) {
        AccidentReportingview.AccedentReport accedentReport = data.get(position);
        holder.Name.setText(accedentReport.Name);
        holder.liscence.setText(accedentReport.registration_no);
        holder.phone.setText(accedentReport.challan_time);

//        String images = accedentReport.images;



        Glide.with(mContext).load(accedentReport.image_paths.get(0)).into(holder.imageView);
        Glide.with(mContext).load(accedentReport.image_paths.get(1)).into(holder.imageView2);
        Glide.with(mContext).load(accedentReport.image_paths.get(2)).into(holder.imageview3);

        //Setup picaoo Here

    }

    @Override
    public int getItemCount() {
        if(data==null){
            return 0;
        }
        return data.size();
    }
}

