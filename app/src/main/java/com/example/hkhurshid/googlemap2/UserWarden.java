package com.example.hkhurshid.googlemap2;

import com.google.gson.Gson;

import java.sql.Time;

/**
 * Created by h.khurshid on 12/11/2017.
 */

public class UserWarden {
    public String Namme;
    public String ID;
    public String RoleName;
    public Double RoleId;
    public String Shift_Name;
    public String Shift_Start;
    public String Shift_End;
    public String LoginTime;


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
