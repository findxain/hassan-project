package com.example.hkhurshid.googlemap2.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Picture;
import android.os.Environment;

import com.example.hkhurshid.googlemap2.DoAccidentReport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by Zain on 12/24/2017.
 */

public class Utils {
    public static String SaveBitmapToFile(Context context, Bitmap bitmap) {

        File file=new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),System.currentTimeMillis()+".png");
        OutputStream outPutStream= null;
        try {
            outPutStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG,10,outPutStream);
            bitmap.recycle();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        return file.getAbsolutePath();
    }
}
