package com.example.hkhurshid.googlemap2;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class incident_view_adapter extends RecyclerView.Adapter<incident_view_adapter.Incviewholder> {




    private Context mContext;;
    private ArrayList<Incident_View_Reporting_Grid.Incidentreports> data;


    public incident_view_adapter(Context mContext) {
        this.mContext = mContext;
    }



    public void setData(ArrayList<Incident_View_Reporting_Grid.Incidentreports> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    @Override
    public incident_view_adapter.Incviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_view_accident_reporting, parent, false);

        return new Incviewholder(itemView);

    }

    @Override
    public void onBindViewHolder(incident_view_adapter.Incviewholder holder, int position) {



        Incident_View_Reporting_Grid.Incidentreports Incidentreports = data.get(position);
        holder.Severity.setText(Incidentreports.severity_level);

// String images = accedentReport.images;
        Glide.with(mContext).load(Incidentreports.image_paths.get(0)).into(holder.image1);
        Glide.with(mContext).load(Incidentreports.image_paths.get(1)).into(holder.image2);
        Glide.with(mContext).load(Incidentreports.image_paths.get(2)).into(holder.image3);
        //Setup picaoo Here

    }

    @Override
    public int getItemCount() {
        if(data==null){
            return 0;
        }
        return data.size();

    }

    public class Incviewholder extends RecyclerView.ViewHolder {
        public TextView   Severity;
        public ImageView image1, image2,image3;


        public Incviewholder(View itemView) {
            super(itemView);
//            Name = (TextView) itemView.findViewById(R.id.textViewName_inc);
            Severity = (TextView) itemView.findViewById(R.id.severitytextViewName);
            image1 = (ImageView) itemView.findViewById(R.id.inc_imageView);
            image2 = (ImageView) itemView.findViewById(R.id.inc_imageView2);
            image3 = (ImageView) itemView.findViewById(R.id.inc_imageView3);



        }
    }
}
