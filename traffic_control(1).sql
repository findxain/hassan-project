-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2017 at 03:16 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `traffic_control`
--

-- --------------------------------------------------------

--
-- Table structure for table `assigned_zones_drivers`
--

CREATE TABLE `assigned_zones_drivers` (
  `id` bigint(20) NOT NULL,
  `zone_id` bigint(20) NOT NULL,
  `driver_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assigned_zones_warden`
--

CREATE TABLE `assigned_zones_warden` (
  `id` bigint(20) NOT NULL,
  `zone_id` bigint(20) NOT NULL,
  `warden_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assigned_zones_warden`
--

INSERT INTO `assigned_zones_warden` (`id`, `zone_id`, `warden_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 4, 1),
(4, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `challan`
--

CREATE TABLE `challan` (
  `id` int(200) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `Challan_cat` varchar(200) DEFAULT NULL,
  `Owner_id` int(200) DEFAULT NULL,
  `warden_id` int(11) NOT NULL,
  `challan_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challan`
--

INSERT INTO `challan` (`id`, `Name`, `Challan_cat`, `Owner_id`, `warden_id`, `challan_time`) VALUES
(1, NULL, '1', 1, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `challan_category`
--

CREATE TABLE `challan_category` (
  `id` int(200) NOT NULL,
  `challan_type` varchar(200) DEFAULT NULL,
  `challan_amount` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challan_category`
--

INSERT INTO `challan_category` (`id`, `challan_type`, `challan_amount`) VALUES
(1, 'route violation', '1500'),
(2, 'test', '200'),
(3, 'test', '200'),
(4, 'fsdfsdtest', '200');

-- --------------------------------------------------------

--
-- Table structure for table `challan_paid`
--

CREATE TABLE `challan_paid` (
  `id` int(200) NOT NULL,
  `challan_cat_id` int(200) DEFAULT NULL,
  `vehicle_id` int(200) DEFAULT NULL,
  `owner_id` int(200) DEFAULT NULL,
  `paid_amount` int(200) DEFAULT NULL,
  `date` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE `owner` (
  `id` int(200) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Cnic_No` int(200) DEFAULT NULL,
  `Liscence_No` int(200) DEFAULT NULL,
  `is_driver` varchar(200) DEFAULT NULL,
  `is_owner` varchar(200) DEFAULT NULL,
  `phone_number` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`id`, `Name`, `password`, `Email`, `Cnic_No`, `Liscence_No`, `is_driver`, `is_owner`, `phone_number`) VALUES
(1, 'hassan', '123456', 'abc@yahoo.com', 370222, 2147483647, '1', '1', 51);

-- --------------------------------------------------------

--
-- Table structure for table `owner_vehicle`
--

CREATE TABLE `owner_vehicle` (
  `id` int(200) NOT NULL,
  `Owner_id` int(200) DEFAULT NULL,
  `Vehicle_id` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `restricted_route`
--

CREATE TABLE `restricted_route` (
  `id` int(200) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `Latitude` varchar(200) DEFAULT NULL,
  `Longitude` varchar(200) DEFAULT NULL,
  `Vehicle_Category` varchar(200) DEFAULT NULL,
  `vehicle_id` int(11) NOT NULL,
  `zone` varchar(100) DEFAULT NULL,
  `warden_id` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restricted_route`
--

INSERT INTO `restricted_route` (`id`, `Name`, `Latitude`, `Longitude`, `Vehicle_Category`, `vehicle_id`, `zone`, `warden_id`) VALUES
(1, 'Murre road ', NULL, NULL, 'bus,truck,double_dacker', 0, 'A', 1),
(2, 'stadium road', NULL, NULL, 'truck', 0, 'A', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_warden_zones`
--

CREATE TABLE `tbl_warden_zones` (
  `id` bigint(20) NOT NULL,
  `zone_name` varchar(10000) NOT NULL,
  `point_A_lat` double NOT NULL,
  `point_A_lng` double NOT NULL,
  `point_B_lat` double NOT NULL,
  `point_B_lng` double NOT NULL,
  `point_C_lat` double NOT NULL,
  `point_C_lng` double NOT NULL,
  `point_D_lat` double NOT NULL,
  `point_D_lng` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_warden_zones`
--

INSERT INTO `tbl_warden_zones` (`id`, `zone_name`, `point_A_lat`, `point_A_lng`, `point_B_lat`, `point_B_lng`, `point_C_lat`, `point_C_lng`, `point_D_lat`, `point_D_lng`) VALUES
(1, 'Zone One', 33.7034903538216, 73.0080585472751, 33.6900295899936, 73.0184011452366, 33.6996701391574, 73.0362539284397, 33.7129152034899, 73.0262975685764),
(2, 'Zone F7', 33.7226192183777, 73.0436439512414, 33.7087687537306, 73.0539436338586, 33.7180502398893, 73.0724830625695, 33.7316136840885, 73.0618400571984);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_zones`
--

CREATE TABLE `tbl_zones` (
  `id` bigint(20) NOT NULL,
  `zone_name` varchar(200) NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_zones`
--

INSERT INTO `tbl_zones` (`id`, `zone_name`, `latitude`, `longitude`) VALUES
(1, 'Pindi', '33.5984', '73.0441'),
(2, 'Pindi', '33.5984', '73.0441'),
(3, 'Isb', '33.7294', '73.0931'),
(4, 'Isb', '33.7294', '73.0931');

-- --------------------------------------------------------

--
-- Table structure for table `traffic_warden`
--

CREATE TABLE `traffic_warden` (
  `id` int(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `zone` varchar(100) DEFAULT NULL,
  `Warden_Shift` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `traffic_warden`
--

INSERT INTO `traffic_warden` (`id`, `Name`, `password`, `zone`, `Warden_Shift`) VALUES
(1, 'haris', 'haris123', '1', ''),
(2, 'Warden2', 'Warden2', '2', ''),
(3, 'Warden3', 'Warden3', '3', '');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(200) NOT NULL,
  `registration_no` varchar(200) DEFAULT NULL,
  `permit_no` varchar(200) DEFAULT NULL,
  `color` text,
  `vehicle_category` text,
  `is_assigned` varchar(200) DEFAULT NULL,
  `zone_id` bigint(20) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `registration_no`, `permit_no`, `color`, `vehicle_category`, `is_assigned`, `zone_id`, `latitude`, `longitude`) VALUES
(1, 'Rir_1323', 'per_!', 'Red', 'truck', '1', 1, 33.5984, 73.0441),
(2, '1234', '1234', 'red', '1', '1', 1, 33.7294, 33.0971),
(3, '12345', 'dfnbck', 'green', 'test', '1', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_assigned`
--

CREATE TABLE `vehicle_assigned` (
  `id` int(200) NOT NULL,
  `vehicle_id` int(200) DEFAULT NULL,
  `owner_id` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_assigned`
--

INSERT INTO `vehicle_assigned` (`id`, `vehicle_id`, `owner_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_category`
--

CREATE TABLE `vehicle_category` (
  `id` int(200) NOT NULL,
  `type` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_category`
--

INSERT INTO `vehicle_category` (`id`, `type`) VALUES
(1, 'truck');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assigned_zones_drivers`
--
ALTER TABLE `assigned_zones_drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assigned_zones_warden`
--
ALTER TABLE `assigned_zones_warden`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan`
--
ALTER TABLE `challan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_category`
--
ALTER TABLE `challan_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_paid`
--
ALTER TABLE `challan_paid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_vehicle`
--
ALTER TABLE `owner_vehicle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restricted_route`
--
ALTER TABLE `restricted_route`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_warden_zones`
--
ALTER TABLE `tbl_warden_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_zones`
--
ALTER TABLE `tbl_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `traffic_warden`
--
ALTER TABLE `traffic_warden`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_assigned`
--
ALTER TABLE `vehicle_assigned`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_category`
--
ALTER TABLE `vehicle_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assigned_zones_drivers`
--
ALTER TABLE `assigned_zones_drivers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assigned_zones_warden`
--
ALTER TABLE `assigned_zones_warden`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `challan`
--
ALTER TABLE `challan`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `challan_category`
--
ALTER TABLE `challan_category`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `challan_paid`
--
ALTER TABLE `challan_paid`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `owner_vehicle`
--
ALTER TABLE `owner_vehicle`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `restricted_route`
--
ALTER TABLE `restricted_route`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_warden_zones`
--
ALTER TABLE `tbl_warden_zones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_zones`
--
ALTER TABLE `tbl_zones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `traffic_warden`
--
ALTER TABLE `traffic_warden`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vehicle_assigned`
--
ALTER TABLE `vehicle_assigned`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vehicle_category`
--
ALTER TABLE `vehicle_category`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
