-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2017 at 06:13 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `traffic_voilation_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `challan`
--

CREATE TABLE `challan` (
  `id` int(200) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `Challan_cat` varchar(200) DEFAULT NULL,
  `Owner_id` int(200) DEFAULT NULL,
  `warden_id` int(11) NOT NULL,
  `challan_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challan`
--

INSERT INTO `challan` (`id`, `Name`, `Challan_cat`, `Owner_id`, `warden_id`, `challan_time`) VALUES
(1, NULL, '1', 1, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `challan_category`
--

CREATE TABLE `challan_category` (
  `id` int(200) NOT NULL,
  `challan_type` varchar(200) DEFAULT NULL,
  `challan_amount` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challan_category`
--

INSERT INTO `challan_category` (`id`, `challan_type`, `challan_amount`) VALUES
(1, 'route violation', '1500');

-- --------------------------------------------------------

--
-- Table structure for table `challan_paid`
--

CREATE TABLE `challan_paid` (
  `id` int(200) NOT NULL,
  `challan_cat_id` int(200) DEFAULT NULL,
  `vehicle_id` int(200) DEFAULT NULL,
  `owner_id` int(200) DEFAULT NULL,
  `paid_amount` int(200) DEFAULT NULL,
  `date` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE `owner` (
  `id` int(200) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Cnic_No` int(200) DEFAULT NULL,
  `Liscence_No` int(200) DEFAULT NULL,
  `is_driver` varchar(200) DEFAULT NULL,
  `is_owner` varchar(200) DEFAULT NULL,
  `phone_number` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`id`, `Name`, `password`, `Email`, `Cnic_No`, `Liscence_No`, `is_driver`, `is_owner`, `phone_number`) VALUES
(1, 'hassan', '123456', 'abc@yahoo.com', 370222, 2147483647, '1', '1', 51);

-- --------------------------------------------------------

--
-- Table structure for table `owner_vehicle`
--

CREATE TABLE `owner_vehicle` (
  `id` int(200) NOT NULL,
  `Owner_id` int(200) DEFAULT NULL,
  `Vehicle_id` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `restricted_route`
--

CREATE TABLE `restricted_route` (
  `id` int(200) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `Latitude` varchar(200) DEFAULT NULL,
  `Longitude` varchar(200) DEFAULT NULL,
  `Vehicle_Category` varchar(200) DEFAULT NULL,
  `vehicle_id` int(11) NOT NULL,
  `zone` varchar(100) DEFAULT NULL,
  `warden_id` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restricted_route`
--

INSERT INTO `restricted_route` (`id`, `Name`, `Latitude`, `Longitude`, `Vehicle_Category`, `vehicle_id`, `zone`, `warden_id`) VALUES
(1, 'Murre road ', NULL, NULL, 'bus,truck,double_dacker', 0, 'A', 1),
(2, 'stadium road', NULL, NULL, 'truck', 0, 'A', 1);

-- --------------------------------------------------------

--
-- Table structure for table `traffic_warden`
--

CREATE TABLE `traffic_warden` (
  `id` int(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `zone` varchar(100) DEFAULT NULL,
  `Warden_Shift` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `traffic_warden`
--

INSERT INTO `traffic_warden` (`id`, `Name`, `password`, `zone`, `Warden_Shift`) VALUES
(1, 'Warden1', 'Warden1', '1', ''),
(2, 'Warden2', 'Warden2', '2', ''),
(3, 'Warden3', 'Warden3', '3', '');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(200) NOT NULL,
  `registration_no` varchar(200) DEFAULT NULL,
  `permit_no` varchar(200) DEFAULT NULL,
  `color` text,
  `vehicle_category` text,
  `is_assigned` varchar(200) DEFAULT NULL,
  `Longitude` varchar(100) NOT NULL,
  `Latitude` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `registration_no`, `permit_no`, `color`, `vehicle_category`, `is_assigned`, `Longitude`, `Latitude`) VALUES
(1, 'Rir_1323', 'per_!', 'Red', 'truck', '1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_assigned`
--

CREATE TABLE `vehicle_assigned` (
  `id` int(200) NOT NULL,
  `vehicle_id` int(200) DEFAULT NULL,
  `owner_id` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_assigned`
--

INSERT INTO `vehicle_assigned` (`id`, `vehicle_id`, `owner_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_category`
--

CREATE TABLE `vehicle_category` (
  `id` int(200) NOT NULL,
  `type` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_category`
--

INSERT INTO `vehicle_category` (`id`, `type`) VALUES
(1, 'truck');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `challan`
--
ALTER TABLE `challan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_category`
--
ALTER TABLE `challan_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_paid`
--
ALTER TABLE `challan_paid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_vehicle`
--
ALTER TABLE `owner_vehicle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restricted_route`
--
ALTER TABLE `restricted_route`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `traffic_warden`
--
ALTER TABLE `traffic_warden`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_assigned`
--
ALTER TABLE `vehicle_assigned`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_category`
--
ALTER TABLE `vehicle_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `challan`
--
ALTER TABLE `challan`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `challan_category`
--
ALTER TABLE `challan_category`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `challan_paid`
--
ALTER TABLE `challan_paid`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `owner_vehicle`
--
ALTER TABLE `owner_vehicle`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `restricted_route`
--
ALTER TABLE `restricted_route`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `traffic_warden`
--
ALTER TABLE `traffic_warden`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vehicle_assigned`
--
ALTER TABLE `vehicle_assigned`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vehicle_category`
--
ALTER TABLE `vehicle_category`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
