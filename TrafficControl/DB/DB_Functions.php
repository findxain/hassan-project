<?php
/**
 * Created by PhpStorm.
 * User: h.khurshid
 * Date: 12/6/2017
 * Time: 7:09 AM
 */

class DB_Functions {

    private $conn;

    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }

    // destructor
    function __destruct() {

    }

    public  function SelectRecord($query){
        $result = $this->conn->query($query);

        $result=$result->fetch_assoc();


            if (count($result)) {
                return $result;
            }else{

                return false;
            }




    }
    public function SelectMulti($query){

        $result = $this->conn->query($query);

        if($result!=null){
            return $result;
        }else{
            return false;
        }
    }
    public function InsertRecord($table,$colls,$vals){
       $query="INSERT INTO $table $colls VALUES $vals";
        $result = $this->conn->query($query);
      if($result){
        return true;
      }else{
          return false;
      }
    }


}

?>