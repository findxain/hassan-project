<?php
/**
 * Created by PhpStorm.
 * User: h.khurshid
 * Date: 12/6/2017
 * Time: 7:07 AM
 */
class DB_Connect {
    private $conn;

    // Connecting to database
    public function connect() {
        require_once '../Config.php';

        // Connecting to mysql database
        $this->conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

        // return database handler
        if( $this->conn->connect_errno){
            printf("Connect failed with database: %s\n", $this->conn->connect_error);
            exit();
        }else{
            return $this->conn;
        }
    }

}